import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  loveIts: number
  created_at: Date

  @Input() postTitle: string;
  @Input() postContent: string;

  constructor() { }

  ngOnInit() {
    // On initialise nos variables ici, on ne se prend pas la tête pour la date, on met
    // La même partout
    this.loveIts = 0;
    this.created_at = new Date(Date.now());
  }

  addLoveIts() {
    this.loveIts += 1;
  }

  removeLoveIts() {
    this.loveIts -= 1;
  }
}
